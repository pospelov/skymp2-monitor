FROM nginx:alpine
COPY default.conf /etc/nginx/conf.d/default.conf
COPY index.html /usr/share/nginx/html/index.html
COPY index.js /usr/share/nginx/html/index.js
COPY styles.css /usr/share/nginx/html/styles.css
COPY FuturaBookC.woff /usr/share/nginx/html/FuturaBookC.woff
COPY crashstat.html /usr/share/nginx/html/crashstat.html
COPY crashstat.js /usr/share/nginx/html/crashstat.js
