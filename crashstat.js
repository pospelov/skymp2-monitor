"use strict";

var BG_COLOR = 'rgba(255, 99, 132, 0.2)';
var BORDER_COLOR = 'rgba(255,99,132,1)';
var BG_COLOR_TWO = 'rgba(75, 192, 192, 0.2)';
var BORDER_COLOR_TWO = 'rgba(75, 192, 192, 1)';
var BG_COLOR_THREE = 'rgba(196, 156, 119, 0.2)';
var BORDER_COLOR_THREE = 'rgba(196, 156, 119, 1)';

var g_data = null;

var canvas = document.getElementById("yourCanvasId");
canvas.width = 1000;
canvas.height = 400;
var ctx = canvas.getContext("2d");
var myLineChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: [],
    datasets: [{
      label: '',
      data: [],
      backgroundColor: [],
      borderColor: [],
      borderWidth: 1
    }, {
      label: '',
      data: [],
      backgroundColor: [],
      borderColor: [],
      borderWidth: 1
    }, {
      label: '',
      data: [],
      backgroundColor: [],
      borderColor: [],
      borderWidth: 1
    }]
  },
  options: {}
});

function mult(val, n) {
  var res = [];
  while (n > 0) {
    --n;
    res.push(val);
  }
  return res;
}

function formatLabel(date) {
  var timestr = date.toTimeString().split(' ')[0];
  var datestr = date.toDateString().split(' ');
  datestr.pop();
  datestr = datestr.reverse();
  datestr.pop();
  datestr = datestr.join(' ');
  return (timestr + ' ' + datestr);
}

function getCrashesCount(crashes) {
  return crashes.length;
}

function getCrashesAvgMinutes(crashes) {
  var sumSecond = 0;
  var sumCrashes = crashes.length;
  //return sumCrashes;
  for (var i = 0; i < crashes.length; ++i) {
    sumSecond += crashes[i].onlineDuration;
  }
  var avgMins = Math.floor(sumSecond / sumCrashes / 60 * 100) / 100;
  if (avgMins !== avgMins) return 0;
  return avgMins;
}

function recalcChart(data, numDays, doShakal) {
  if (!numDays) numDays = 1;
  console.log('recalcChart', numDays, doShakal)

  var allCrases = [];
  var allSessions = [];
  var latestCrashDate = -1;
  data.forEach(function (player) {
    player.sessions.forEach(function (crash) {
      if (!crash.version) crash.version = 'Unknown';
      if (!crash.isNormalExit) allCrases.push(crash);
      allSessions.push(crash);
      var dt = (+new Date(crash.date));
      if (latestCrashDate < dt) latestCrashDate = dt;
    });
  });
  latestCrashDate = new Date(latestCrashDate);

  // https://stackoverflow.com/questions/10123953/sort-javascript-object-array-by-date
  // Turn your strings into dates, and then subtract them
  // to get a value that is either negative, positive, or zero.
  var fnSort = function (a, b) { return new Date(a.date) - new Date(b.date); };
  allCrases.sort(fnSort);
  allSessions.sort(fnSort);

  var chart = myLineChart;
  var labels = [];
  console.log(+latestCrashDate);
  for (var i = 1 - 24 * numDays; i <= 0; ++i) {
    var d = Date.now() -(Date.now() - +latestCrashDate) + (i * 3600 * 1000);
    d = Math.floor(d / 3600 / 1000) * 3600 * 1000;
    labels.push(new Date(d));
  }

  var crashArray = [];
  while (crashArray.length < labels.length) crashArray.push([]);
  allCrases.forEach(function (crash) {
    for (var i = 0; i < labels.length - 1; ++i) {
      if (new Date(crash.date) >= labels[i] && new Date(crash.date) < labels[i + 1]) {
        crashArray[i].push(crash);
      }
    }
  });
  var crashArrayOrigin = crashArray;
  crashArray = crashArray.map(getCrashesAvgMinutes);
  var crashSumArray = crashArrayOrigin.map(getCrashesCount);

  var onlineArray = [];
  var getOnline = function (date) {
    date = +date;
    var getOnlineRet = 0;
    allSessions.forEach(function (session) {
      var lastLogin = +(new Date(session.date)) - 1000 * session.onlineDuration;
      var lastUpdate = +(new Date(session.date));
      if (date >= lastLogin && date <= lastUpdate) getOnlineRet++;
    });
    return getOnlineRet;
  }
  for (var i = 0; i < labels.length; ++i) {
    var dateHour = +labels[i];
    var minOnlineSum = 0;
    for (var m = 0; m < 60; ++m) {
      var dateMin = +dateHour + (m * 60 * 1000);
      minOnlineSum += getOnline(dateMin);
    }
    var hourOnline = minOnlineSum / 60;
    onlineArray.push(Math.floor(hourOnline * 100) / 100);
  }

  chart.data.labels = labels.map(formatLabel); // 04:48:57 23 Nov
  for (var i = 0; i < labels.length; ++i) {
    var versions = {};
    crashArrayOrigin[i].forEach(function (crash) {
      if (!versions[crash.version]) versions[crash.version] = 0;
      versions[crash.version]++;
    });
    var topVersion = '';
    var topVersionN = -1;
    Object.keys(versions).forEach(function (version) {
      if (versions[version] > topVersionN) {
        topVersionN = versions[version];
        topVersion = '[' + version + ']';
      }
    });
    chart.data.labels[i] = topVersion + ' ' + chart.data.labels[i];
  }

  // На этом моменте в crashSumArray лежит сумма вылетов за каждый час
  // А мы хотим поделить это на онлайн за текущий час:
  for (var hour = 0; hour < crashSumArray.length; ++hour) {
    crashSumArray[hour] /= onlineArray[hour];
    if (onlineArray[hour] == 0) crashSumArray[hour] = 0;
  }

  var shakalTransform = function (data, shakalN) {
    var newData = [];
    if (data.length % shakalN != 0) throw new Error('Unable to shakal data: shakalN=' + shakalN + ' data.length=' + data.length);
    for (var k = 0; k < (data.length / shakalN); ++k) {

      var valuesSum = 0; // count is shakalN
      for (var shi = 0; shi < shakalN; ++shi) {
        valuesSum += data[k + shi];
      }
      newData.push(valuesSum / shakalN);
    }
    return newData.map(function (x) { return (Math.floor(x * 100) / 100) });
  };

  if (doShakal) {
    crashArray = shakalTransform(crashArray, 24);
    onlineArray = shakalTransform(onlineArray, 24);
    crashSumArray = shakalTransform(crashSumArray, 24);
    var newLabels = [];
    for (var i = 0; i < chart.data.labels.length; ++i) {
      if (i % 24 == 0) newLabels.push(chart.data.labels[i]);
    }
    chart.data.labels = newLabels;
  }

  //chart.data.labels = shakalTransform(chart.data.labels, 24, true);

  // среднее время до вылета в минутах
  chart.data.datasets[1].label = 'Среднее время до вылета в минутах';
  chart.data.datasets[1].data = crashArray;
  console.log('crashArray', crashArray);
  var n = chart.data.datasets[1].data.length;
  chart.data.datasets[1].backgroundColor = mult(BG_COLOR, n);
  chart.data.datasets[1].borderColor = mult(BORDER_COLOR, n);

  // средний онлайн
  chart.data.datasets[0].label = 'Средний онлайн';
  chart.data.datasets[0].data = onlineArray;
  console.log('onlineArray', onlineArray);
  chart.data.datasets[0].backgroundColor = mult(BG_COLOR_TWO, n);
  chart.data.datasets[0].borderColor = mult(BORDER_COLOR_TWO, n);

  // вылетов в час
  chart.data.datasets[2].label = 'Вылетов в час на единицу онлайна';
  chart.data.datasets[2].data = crashSumArray;
  console.log('crashSumArray', crashSumArray);
  chart.data.datasets[2].backgroundColor = mult(BG_COLOR_THREE, n);
  chart.data.datasets[2].borderColor = mult(BORDER_COLOR_THREE, n);

  myLineChart.update();
}

setTimeout(function () {
  var addr = localStorage.getItem('serverAddress');
  axios.get('http://' + addr + '/api/users').then(function (response) {
    recalcChart(response.data);
    g_data = response.data;
  }).catch(function (e) {
    console.error(e);
  });
}, 1);


var g_lastCountValue = null;
var g_lastPerDayValue = null;
setInterval(function () {
  myLineChart.resize();

  if (!g_data) return;

  var countValue = document.getElementById("countId").value;
  var perDayValue = document.getElementById("perDayId").checked;
  if (countValue != g_lastCountValue) {
    g_lastCountValue = countValue;
    //console.log(countValue);
    recalcChart(g_data, g_lastCountValue, g_lastPerDayValue);
  }
  if (perDayValue != g_lastPerDayValue) {
    g_lastPerDayValue = perDayValue;
    //сonsole.log(countValue);
    recalcChart(g_data, g_lastCountValue, g_lastPerDayValue);
  }
}, 100);
