'use strict';

var g_lastRequest;

function formatDuration(duration) {
  var h = 0,
      m = 0,
      s = +duration;
  while (s >= 60) {
    s -= 60;
    m++;
  }
  while (m >= 60) {
    m -= 60;
    h++;
  }
  var str = s + ' сек ';
  if (m > 0 || h > 0) str = m + ' мин ' + str;
  if (h > 0) str = h + ' ч ' + str;
  return str;
}

function updateUser(user) {
  user._totalOnlineStr = formatDuration(user.totalOnlineDuration);
}

var app = new Vue({
  el: '#app',
  data: {
    message: axios,
    serverAddress: localStorage.getItem('serverAddress') || '185.211.246.231:7777',
    serverAddressWas: '',
    users: [],
    chart: true,
    table: false,
    online: 0
  },
  mounted: function mounted() {
    this.startInterval();
  },
  methods: {
    startInterval: function startInterval() {
      var _this = this;

      setInterval(function () {
        if (_this.serverAddress != _this.serverAddressWas && (!g_lastRequest || Date.now() - g_lastRequest > 5000)) {
          _this.serverAddressWas = _this.serverAddress;
          g_lastRequest = Date.now();
          _this.onServerChange(_this.serverAddress);
        }
      }, 100);
      setInterval(function () {
        _this.users.forEach(function (user) {
          if (user.online) {
            user.totalOnlineDuration++;
            console.log('++');
          }
          updateUser(user);
          _this.redrawUsers();
        });
      }, 1000);
    },
    onServerChange: function onServerChange(addr) {
      var _this2 = this;

      localStorage.setItem('serverAddress', addr);
      //console.log('changed to ', addr)
      axios.get('http://' + addr + '/api/users').then(function (response) {
        _this2.users = response.data;
        _this2.users.forEach(updateUser);

        _this2.online = 0;
        for (var i = 0; i < _this2.users.length; ++i) {
          if (_this2.users[i].online) _this2.online++;
        }

        _this2.redrawUsers();
        _this2.serverAddressWas = ''; // force reload users
        //console.log('loaded ' + this.users.length + ' users');
      }).catch(function (e) {
        console.error(e);
        _this2.users = [];
      });
    },
    redrawUsers: function redrawUsers() {
      this.users = JSON.parse(JSON.stringify(this.users));
    }
  }
});
